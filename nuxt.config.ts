// https://nuxt.com/docs/api/configuration/nuxt-config
export default defineNuxtConfig({
  devtools: { enabled: true },
  modules: [
    '@nuxtjs/i18n',
    '@nuxtjs/tailwindcss',
    '@nuxtjs/google-fonts',
    'nuxt-swiper'
  ],

  swiper: {
    modules: ['navigation', 'pagination'],
  },

  app: {
    pageTransition: { name: 'page', mode: 'out-in' }
  },

  googleFonts: {
    families: {
      Montserrat: {
        wght: [400, 500, 600, 700],
      },
      download: true,
      base64: true,
      inject: true,
      overwriting: false
    }
  },

  tailwindcss: {
    cssPath: '~/assets/css/tailwind.css',
    configPath: 'tailwind.config',
    exposeConfig: false,
    exposeLevel: 2,
    config: {},
    injectPosition: 'first',
    viewer: true,
  },
  
  i18n: { 
    lazy: true,
    langDir: 'locales',
    locales:[
      {
        code: 'ru',
        iso: 'ru-RU',
        name: 'Русский',
        file: 'ru-RU.json'
      },
      {
        code: 'en',
        iso: 'en-US',
        name: 'English(US)',
        file: 'en-US.json'
      },
      {
        code: 'uz',
        iso: 'Lt-uz-UZ',
        name: 'Uzbek',
        file: 'uz-UZB.json'
      },
      {
        code: 'oz',
        iso: 'Cy-uz-UZ',
        name: 'Uzbek',
        file: 'oz-UZB.json'
      },
    ],
    strategy: 'prefix',
    defaultLocale: 'ru',
  }
})
